<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 * Time: 01:19
 *
 * @var $app System\Dispatcher
 */

require 'app/config/main.php';

$app = require './system/App.php';
$app->initiate();