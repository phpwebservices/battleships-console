<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 * Usage: cd app/tests && phpunit ShipTest
 */

namespace App\Tests;

require_once '../system/requireFiles.php';

use App\Models\GameBoardItem;
use App\Models\Ships\Ship;
use PHPUnit\Framework\TestCase;

class ShipTest extends TestCase
{
    private $ships = [
        'App\Models\Ships\Battleship',
        'App\Models\Ships\Destroyer'
    ];

    public function testShips()
    {
        foreach ($this->ships as $ShipName) {
            $this->__ship($ShipName);
        }
    }

    private function __ship($ShipName)
    {
        $ship = new $ShipName('C', 3); /** @var $ship Ship */
        $ship->setRotation(GameBoardItem::HORIZONTAL);
        $ship->buildCoordinates();

        foreach ($ship->range() as $pos) {
            $this->assertTrue($ship->markHit("C{$pos}"), "{$ShipName}->markHit(C$pos) HORIZONTAL hasn't worked this should be a hit.");
        }

        $this->assertTrue($ship->isDestroyed(), "{$ShipName}->isDestroyed() HORIZONTAL hasn't worked this should be destroyed.");

        $ship = new $ShipName('C', 3);
        $ship->setRotation(GameBoardItem::VERTICAL);
        $ship->buildCoordinates();

        foreach ($ship->range() as $pos) {
            $this->assertTrue($ship->markHit("{$pos}3"), "{$ShipName}->markHit($pos, 3) VERTICAL hasn't worked this should be a hit.");
        }

        $this->assertTrue($ship->isDestroyed(), "{$ShipName}->isDestroyed() VERTICAL hasn't worked this should be destroyed.");

        $ship->setRotation(GameBoardItem::HORIZONTAL);
        $this->assertFalse($ship->markHit("A1"), "{$ShipName}->markHit(A1) HORIZONTAL hasn't worked this should not be a hit.");

        $ship->setRotation(GameBoardItem::VERTICAL);
        $this->assertFalse($ship->markHit("A1"), "{$ShipName}->markHit(A1) VERTICAL hasn't worked this should not be a hit.");
    }

}