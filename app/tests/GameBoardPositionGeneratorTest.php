<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 * Usage: cd battleships/app && /usr/bin/php /{PATH_TO_PROJECT}/phpws/vendor/phpunit/phpunit/phpunit /{PATH_TO_PROJECT}/phpws/battleships/app/tests
 */

namespace App\Tests;

require_once './config/main.php';
require_once '../system/requireFiles.php';

use App\Models\BattleshipsBoard;
use App\Models\Collections\BoardItemsCollection;
use App\Models\Collections\GridItemCollection;
use App\Models\Drivers\FileDriverInterface;
use App\Models\Drivers\FileSaveDriver;
use App\Models\GameBoardItem;
use App\Models\GameBoardPositionGenerator;
use App\Models\Ships\Ship;
use PHPUnit\Framework\TestCase;
use System\Data\Drivers;

class GameBoardPositionGeneratorTest extends TestCase
{
    private array $ships = [
        'App\Models\Ships\Battleship',
        'App\Models\Ships\Destroyer'
    ];

    public function testOverlappingShips()
    {
        foreach ($this->ships as $ShipName) {
            $this->__ship($ShipName);
        }
    }

    private function __ship($ShipName)
    {
        $gameBoard = new BattleshipsBoard(
            10,
            10,
            new Drivers\File(),
            new BoardItemsCollection([]),
            new GridItemCollection([])
        );

        $generator = new GameBoardPositionGenerator($gameBoard);

        $ship1 = new $ShipName('C', 0); /** @var $ship1 Ship */
        $ship1->setRotation(GameBoardItem::HORIZONTAL);
        $ship1->buildCoordinates();

        $gameBoard->addBoardItem($ship1);

        $ship2 = new $ShipName('C', 0); /** @var $ship2 Ship */
        $ship2->setRotation(GameBoardItem::VERTICAL);
        $ship2->buildCoordinates();

        $overLapShip = $gameBoard->getBoardItems()->positionMatch($ship2->getRowPosition().$ship2->getColPosition());

        $this->assertTrue($overLapShip instanceof Ship, "positionMatch check is failing ship1 and ship2 are in the same range this should assert true");

        $generator->positionRandomItem($ship2);
    }

}