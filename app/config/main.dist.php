<?php

define('APP_ROOT', '/path/to/battleships/');

/**
 * TODO:: Move config folder to its own repository and move it outside of /var/www with restricted permissions
 * note this only needs to be done when the config file holds sensitive data, at the moment it doesn't.
 */
define('SITE_SCHEME', 'http');
define('SITE_DOMAIN', 'phpws.uk');
define('FULL_DOMAIN', SITE_SCHEME.'://'.SITE_DOMAIN);