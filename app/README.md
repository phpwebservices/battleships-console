# Basic Battleships Demo
### A very simple version of battleships game with no graphics.

---

To view and play the game visit:

* **[phpwebservices/battleships](http://www.phpwebservices.uk/battleships/)**