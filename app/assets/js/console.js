// Note: This doesn't work and needs fixing!
// Can't see a use for it at the moment so not fixing it now.

var CustomHandler = function() {

    var self = this;

    self.id = function(id) {
        return document.getElementById(id)
    };

    self.keypress = function() {
        document.onkeypress = function(e) {
            if (e.ctrlKey || e.which == 3) {
                //key.toggle();
                //localStorage.setItem('isVisible', key.is(":visible"));
            }
        }
    };
};

var ch = new CustomHandler;

var CustomConsole = function () {

    var self = this;

    self.name = 'console-window';
    self.id = '#'+self.name;
    self.className = '.'+self.name;

    self.pos = {};

    self.init = function (clearStore) {

        if (clearStore) {
            self.clearStore();
        }
        if (!localStorage.getItem('debugHeight')) {
            var temp = {};
            temp[self.name] = "182";
            localStorage.setItem('debugHeight', JSON.stringify(temp));
        }
        self.addLoadEvent(self.updateSizes());
        self.addKeyHandler();
        self.startConsole(null);
    };

    self.addLoadEvent = function (func) {
        var oldonload = window.onload;
        if (typeof window.onload != 'function') {
            window.onload = func;
        } else {
            window.onload = function () {
                if (oldonload) {
                    oldonload();
                }
                func();
            }
        }
    };

    self.getMouseXY = function (e) {

        self.pos = e.getBoundingClientRect();

        console.log(self.pos.height);
        console.log(window.innerHeight);
        console.log(window.pageYOffset);

        var size = (window.innerHeight - self.pos.height + window.pageYOffset - 5);
        //self.setSize(size);
        self.updateSize(size);
        return true
    };

    self.setSize = function (size) {
        //var heights = JSON.parse(localStorage.getItem('debugHeight'));
        //heights[self.name] = size;
        //localStorage.setItem('debugHeight', JSON.stringify(heights));
    };

    self.updateSize = function (size) {
        document.getElementById(self.name).style.height = size + 'px';
    };

    self.updateSizes = function () {
        var heights = JSON.parse(localStorage.getItem('debugHeight'));
        var count = 0;
        for (var key in heights) {
            if (document.getElementById(key)) {
                document.getElementById(key).style.height = heights[key] + 'px';
            }
            count++;
        }
    };

    self.resizeDebug = function (id) {
        document.onmousemove = self.getMouseXY(ch.id(id));
    };

    self.stopResize = function () {
        document.onmousemove = new function () {};
    };

    self.startConsole = function (lastId) {

    };

    self.handleData = function (data) {
        if (!data) {
            return null;
        }
        if (typeof data !== 'object') {
            return data;
        }
        if (data.date) {
            return data.date;
        }
        return data.name + ' ' + (data.message ? data.message : '');
    };

    self.clearStore = function() {
        localStorage.removeItem('startId');
        localStorage.removeItem('lastId');
    };

    self.addKeyHandler = function() {
        var key = cc.id(self.id);
        if (localStorage.getItem('isVisible') == 'true') {
            key.toggle();
        }
        cc.keypress();
    };
};

var Console = new CustomConsole();