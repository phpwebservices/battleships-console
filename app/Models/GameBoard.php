<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 * Update: 30/11/19
 */

namespace App\Models;

use App\Models\Collections\GridItemCollection;
use System;
use System\Data\Drivers\Interfaces\DriverInterface;
use App\Models\Collections\BoardItemsCollection;

abstract class GameBoard extends System\BaseClass
{
    const GRID_STATUS_BLANK     = 0;
    const GRID_STATUS_EMPTY     = 1;

    const MAX_ROW_SIZE = 26;
    const MAX_COL_SIZE = 52;

    const BOARD_ITEMS_KEY = 'boardItems';

    const MAX_COLS_WITHOUT_SEPARATOR = 10;

    /**
     * @var int
     */
    private int $cols;

    /**
     * @var int
     */
    private int $rows;

    /**
     * @var array
     */
    protected array $colHeaders = [];

    /**
     * @var array
     */
    protected array $rowHeaders = [];

    /**
     * @var DriverInterface
     */
    private ?DriverInterface $dataDriver = null;

    /**
     * @var string
     */
    private string $callingClassName = '';

    /**
     * @var BoardItemsCollection
     */
    protected ?BoardItemsCollection $boardItems;

    /**
     * @var GridItemCollection
     */
    protected ?GridItemCollection $grid = null;

    /**
     * @return bool
     */
    abstract public function isCompleted(): bool;

    /**
     * @param int $cols
     * @param int $rows
     * @param DriverInterface $dataDriver
     * @param BoardItemsCollection $boardItems
     * @param GridItemCollection $gridItemCollection
     *
     * @throws \Exception
     */
    public function __construct(
        int $cols,
        int $rows,
        DriverInterface $dataDriver,
        BoardItemsCollection $boardItems,
        GridItemCollection $gridItemCollection
    )  {
        if(!$this->validateBoardSize($cols, $rows)) {
            throw new \Exception(
                "Board size to large, try again, Max Width: ".self::MAX_COL_SIZE." Max Height: ".self::MAX_ROW_SIZE
            );
        }

        $this->cols = $cols;
        $this->rows = $rows;
        $this->dataDriver = $dataDriver;
        $this->callingClassName = get_class($this);
        $this->boardItems = $boardItems;
        $this->grid = $gridItemCollection;

        $this->buildBoardComponents();
    }

    /**
     * @return GridItemCollection
     */
    public function getGrid(): GridItemCollection
    {
        return $this->grid;
    }

    /**
     * @param string $status
     *
     * @return GridItemCollection
     */
    public function resetBoard($status): GridItemCollection
    {
        $data = [];
        foreach ($this->rowHeaders as $rowId) {
            foreach ($this->colHeaders as $colId) {
                $data[$rowId.$colId] = new GridItem($status, GridItem::TYPE_DEFAULT);
            }
        }
        return new GridItemCollection($data);
    }

    protected function buildBoardComponents(): void
    {
        $this->colHeaders = range(0, $this->cols - 1);

        if ($this->isDoubleSize()) {
            array_walk($this->colHeaders, function (&$item) {
                $item = str_pad($item, 2, '0', STR_PAD_LEFT);
            });
        }

        $this->rowHeaders = range('A', chr(ord('A') + $this->rows - 1));
    }

    /**
     * @param int $cols
     * @param int $rows
     *
     * @return bool
     */
    private function validateBoardSize(int $cols, int $rows): bool
    {
        if ($cols > self::MAX_COL_SIZE) {
            return false;
        }
        if ($rows > self::MAX_ROW_SIZE) {
            return false;
        }
        return true;
    }

    public function reset(): void
    {
        $this->grid = $this->resetBoard(GameBoard::GRID_STATUS_EMPTY);
        $this->dataDriver->reset($this->callingClassName);
        $this->dataDriver->reset(self::BOARD_ITEMS_KEY);
    }

    /**
     * @return bool
     */
    public function restore(): bool
    {
        $grid = $this->dataDriver->restore($this->callingClassName);
        $this->boardItems = $this->dataDriver->restore(self::BOARD_ITEMS_KEY);

        if ($grid instanceof GridItemCollection && $this->boardItems instanceof BoardItemsCollection) {
            $this->grid = $grid;
            return true;
        }

        return false;
    }

    /**
     * @param string $gridId
     * @param string $status
     * @param string $type
     *
     * @throws \Exception
     */
    public function setGridState(string $gridId, string $status, string $type = ''): void
    {
        $gridId = strtoupper($gridId);

        if (!$this->grid->hasCoordinates($gridId)) {
            throw new \Exception("Error grid reference is out of range, try again");
        }

        $this->grid->updateData(
            $gridId,
            new GridItem($status, $type)
        );
    }

    /**
     * @return bool
     */
    public function isDoubleSize(): bool
    {
        return $this->cols > self::MAX_COLS_WITHOUT_SEPARATOR;
    }

    /**
     * @return float
     */
    public function ratioOfBoardSize(): float
    {
        $scaleFactor = 0.98;
        if ($this->cols < $this->rows) {
            $scaleFactor = 1.05;
        } elseif ($this->cols == $this->rows) {
            $scaleFactor = 1.2;
        }
        $ratios = max([$this->cols / self::MAX_COL_SIZE, $this->rows / self::MAX_ROW_SIZE]);

        return $scaleFactor / $ratios;
    }

    /**
     * @return array
     */
    public function getColHeaders(): array
    {
        return $this->colHeaders;
    }

    /**
     * @return array
     */
    public function getRowHeaders(): array
    {
        return $this->rowHeaders;
    }

    /**
     * @return BoardItemsCollection
     */
    public function getBoardItems(): BoardItemsCollection
    {
        return $this->boardItems;
    }

    /**
     * @param GameBoardItem $boardItem
     */
    public function addBoardItem(GameBoardItem $boardItem): void
    {
        $this->boardItems->addData($boardItem);
    }

    public function __destruct()
    {
        $this->dataDriver->save($this->callingClassName, $this->grid);
        $this->dataDriver->save(self::BOARD_ITEMS_KEY, $this->boardItems);
    }
}