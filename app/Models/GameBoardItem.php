<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models;

use System;

abstract class GameBoardItem extends System\BaseClass
{
    const HORIZONTAL = 0;
    const VERTICAL = 1;

    /**
     * @var string
     */
    protected string $row = '';

    /**
     * @var string
     */
    protected string $col = "-1";

    /**
     * @var bool
     */
    protected bool $isDoubleDigits = false;

    /**
     * @var int
     */
    protected int $itemLength = 1;

    /**
     * @var int
     */
    protected int $rotation = 0;

    /**
     * @var array
     */
    private array $coordinates = [];

    public function __construct($row = 'A', $col = 0)
    {
        $this->row = $row;
        $this->col = $col;
    }

    abstract public function type(): string;

    /**
     * @param string $row
     */
    public function setRow($row)
    {
        $this->row = $row;
    }

    /**
     * @param string $col
     */
    public function setCol(string $col)
    {
        $this->col = $col;
    }

    public function setRotation($rotation)
    {
        $this->rotation = $rotation;
    }

    public function getRowPosition()
    {
        return $this->row;
    }

    public function getRangePosition()
    {
        return $this->isVertical() ? $this->getRowRangePosition() : $this->getColRangePosition();
    }

    protected function getRowRangePosition()
    {
        return chr(ord($this->row) + $this->itemLength - 1);
    }

    public function getColPosition(): string
    {
        return $this->col;
    }

    protected function getColRangePosition()
    {
        return ((int)$this->col) + $this->itemLength - 1;
    }

    /**
     * @return int
     */
    public function getItemLength()
    {
        return $this->itemLength;
    }

    /**
     * @return int
     */
    public function isHorizontal()
    {
        return $this->rotation == self::HORIZONTAL;
    }

    /**
     * @return bool
     */
    public function isVertical()
    {
        return $this->rotation == self::VERTICAL;
    }

    /**
     * @return array
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    public function resetCoordinates()
    {
        $this->coordinates = [];
    }

    /**
     * @param $position
     *
     * @return bool
     */
    public function positionMatch($position)
    {
        return in_array($position, $this->coordinates);
    }

    public function buildCoordinates()
    {
        foreach ($this->range() as $data) {
            $this->coordinates[] = $this->isHorizontal() ? "{$this->row}{$data}" : "{$data}{$this->col}";
        }
    }

    /**
     * @param $isDoubleDigits
     */
    public function setDoubleDigits($isDoubleDigits)
    {
        $this->isDoubleDigits = $isDoubleDigits;
    }

    /**
     * @return array
     */
    public function range()
    {
        if ($this->isHorizontal()) {
            $data = range($this->getColPosition(), $this->getColRangePosition());

            if ($this->isDoubleDigits) foreach ($data as &$value) $value = str_pad($value, 2, '0', STR_PAD_LEFT);

            return $data;
        } else {
            return range($this->getRowPosition(), $this->getRowRangePosition());
        }
    }

    public function itemOverlap(GameBoardItem $item)
    {
        return count(array_diff($this->getCoordinates(), $item->getCoordinates())) != count($this->getCoordinates());
    }

}