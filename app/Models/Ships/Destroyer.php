<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models\Ships;

class Destroyer extends Ship
{
    /**
     * @var int
     */
    protected int $itemLength = 4;

    /**
     * @return string
     */
    public function type(): string
    {
        return 'destroyer';
    }
}