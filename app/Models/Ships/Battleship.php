<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models\Ships;

class Battleship extends Ship
{
    /**
     * @var int
     */
    protected int $itemLength = 5;

    /**
     * @return string
     */
    public function type(): string
    {
        return 'battleship';
    }
}