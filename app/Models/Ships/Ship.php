<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models\Ships;

use App\Models\GameBoardItem;

abstract class Ship extends GameBoardItem {

    protected array $hits = [];

    /**
     * @return bool
     */
    public function isDestroyed(): bool
    {
        return count($this->hits) == $this->itemLength;
    }

    /**
     * @param string $position
     *
     * @return bool
     */
    public function markHit(string $position): bool
    {
        if (!$this->positionMatch($position)) {
            return false;
        }
        return ($this->hits[$position] = true);
    }

    /**
     * @param string $position
     *
     * @return bool
     */
    public function isAlreadyHit(string $position): bool
    {
        return isset($this->hits[$position]) ? $this->hits[$position] : false;
    }
}