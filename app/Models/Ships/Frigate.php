<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models\Ships;

class Frigate extends Ship
{
    /**
     * @var int
     */
    protected int $itemLength = 2;

    /**
     * @return string
     */
    public function type(): string
    {
        return 'frigate';
    }
}