<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 * Update: 30/11/19
 */

namespace App\Models;

class GameBoardPositionGenerator
{
    /**
     * @var GameBoard
     */
    private ?GameBoard $board = null;

    /**
     * @var string
     */
    private string $rowEnd = 'Z';

    /**
     * @var int
     */
    private int $colEnd = -1;

    /**
     * GameBoardPositionGenerator constructor.
     *
     * @param GameBoard $board
     */
    public function __construct(GameBoard $board)
    {
        $this->board = $board;
        $this->setRowEnd();
        $this->setColEnd();
    }

    private function setRowEnd(): void
    {
        $rowHeaders = $this->board->getRowHeaders();
        $this->rowEnd = array_pop($rowHeaders);
    }

    private function setColEnd(): void
    {
        $colHeaders = $this->board->getColHeaders();
        $this->colEnd = array_pop($colHeaders);
    }

    /**
     * @param GameBoardItem $item
     *
     * @return GameBoardItem
     */
    public function positionRandomItem(GameBoardItem $item): GameBoardItem
    {
        $item->setRotation($this->getRandomRotation());

        $coords = $this->board->getBoardItems()->getOccupiedCoordinates();

        if ($item->isVertical()) {
            $item = $this->positionVerticalItem($item, $coords);
        } else {
            $item = $this->positionHorizontalItem($item, $coords);
        }

        return $item;
    }

    /**
     * @param GameBoardItem $item
     * @param array $coords
     *
     * @return GameBoardItem
     */
    public function positionVerticalItem(GameBoardItem $item, array &$coords): GameBoardItem
    {
        do {
            $row = chr(rand(ord('A'), ord($this->rowEnd) - $item->getItemLength() + 1));
            $col = rand(0, $this->colEnd);

            if ($this->board->isDoubleSize()) {
                $col = str_pad($col, 2, '0', STR_PAD_LEFT);
            }

        } while(in_array($row.$col, $coords));

        $item->setRow($row);
        $item->setCol($col);

        $item->setDoubleDigits($this->board->isDoubleSize());

        $item->buildCoordinates();

        $diff = array_diff($item->getCoordinates(), $coords);
        if ($diff != $item->getCoordinates()) {
            $item->resetCoordinates();
            return $this->positionVerticalItem($item, $coords);
        }

        return $item;
    }

    /**
     * @param GameBoardItem $item
     * @param array $coords
     *
     * @return GameBoardItem
     */
    public function positionHorizontalItem(GameBoardItem $item, array &$coords): GameBoardItem
    {
        do {
            $row = chr(rand(ord('A'), ord($this->rowEnd)));
            $col = rand(0, $this->colEnd - $item->getItemLength() + 1);

            if ($this->board->isDoubleSize()) {
                $col = str_pad($col, 2, '0', STR_PAD_LEFT);
            }

        } while(in_array($row.$col, $coords));

        $item->setRow($row);
        $item->setCol($col);

        $item->setDoubleDigits($this->board->isDoubleSize());

        $item->buildCoordinates();

        $diff = array_diff($item->getCoordinates(), $coords);

        if ($diff != $item->getCoordinates()) {
            $item->resetCoordinates();
            return $this->positionHorizontalItem($item, $coords);
        }

        return $item;
    }

    /**
     * @return int
     */
    public function getRandomRotation(): int
    {
        return rand(GameBoardItem::HORIZONTAL, GameBoardItem::VERTICAL);
    }

}