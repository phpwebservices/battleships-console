<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models\Collections;

use System;
use App\Models\GameBoardItem;

class BoardItemsCollection extends System\ObjectCollection
{
    /**
     * @param GameBoardItem[] $items
     */
    public function __construct(array $items)
    {
        $this->setData($items);
    }

    /**
     * @param GameBoardItem $itemToCheck
     *
     * @return GameBoardItem|bool
     */
    public function getItemOverlapping(GameBoardItem $itemToCheck)
    {
        foreach ($this as $item) { /** @var $item GameBoardItem */
            if ($item->itemOverlap($itemToCheck)) {
                return $item;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getOccupiedCoordinates(): array
    {
        $coordinates = [];
        foreach ($this as $item) { /** @var $item GameBoardItem */
            $coordinates = array_merge($coordinates, $item->getCoordinates());
        }
        return $coordinates;
    }

    public function positionMatch($position)
    {
        foreach ($this as $item) { /** @var $item GameBoardItem */
            if ($item->positionMatch($position)) {
                return $item;
            }
        }
        return false;
    }

}