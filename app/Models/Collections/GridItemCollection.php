<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models\Collections;

use System;

class GridItemCollection extends System\ObjectCollection
{
    /**
     * @param GameBoardItem[] $items
     */
    public function __construct(array $items)
    {
        $this->setData($items);
    }

    /**
     * @param string $coords
     * @return bool
     */
    public function hasCoordinates(string $coords): bool
    {
        return array_key_exists($coords, $this->iteratorData);
    }
}