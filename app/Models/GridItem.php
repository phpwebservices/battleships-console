<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models;

use System;

class GridItem extends System\BaseClass
{
    /**
     * @var string
     */
    const TYPE_DEFAULT = 1;

    /**
     * @var int
     */
    private int $status;

    /**
     * @var string
     */
    private string $type = '';

    /**
     * GridItem constructor.
     *
     * @param int $status
     * @param string $type
     */
    public function __construct(int $status, string $type)
    {
        $this->status = $status;
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}