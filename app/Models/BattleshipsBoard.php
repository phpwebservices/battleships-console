<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Models;

use App\Models\Collections\GridItemCollection;
use App\Models\Ships\Ship;

class BattleshipsBoard extends GameBoard {

    /**
     * Possible states of a grid
     */
    const GRID_STATUS_MISS  = 2;
    const GRID_STATUS_HIT   = 3;

    const TYPE_MISS = 'miss';

    /**
     * @var int
     */
    private int $numberOfShots = 0;

    /**
     * @return int
     */
    public function getNumberOfShots(): int
    {
        $this->numberOfShots = 0;
        foreach ($this->grid->data() as $item) { /** @var $item GridItem */
            if ($item->getStatus() != self::GRID_STATUS_EMPTY) {
                $this->numberOfShots++;
            }
        }
        return $this->numberOfShots;
    }

    /**
     * @param $position
     *
     * @return GameBoardItem|Ship|bool
     */
    public function positionMatch($position)
    {
        return $this->getBoardItems()->positionMatch($position);
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        foreach($this->getBoardItems() as $ship) { /** @var $ship Ship */
            if (!$ship->isDestroyed()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return GridItemCollection
     */
    public function getHiddenData(): GridItemCollection
    {
        $grid = $this->resetBoard(GameBoard::GRID_STATUS_BLANK);

        $boardItems = $this->getBoardItems();

        foreach ($boardItems as $item) { /** @var $item GameBoardItem */
            $coords = $item->getCoordinates();
            foreach($coords as $cord) {
                $grid->updateData(
                    $cord,
                    new GridItem(self::GRID_STATUS_HIT, $item->type())
                );
            }
        }

        return $grid;
    }

}