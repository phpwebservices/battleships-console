<!doctype html>
<html>
<head>
    <title>phpwebservices.uk - <?= $this->errorCode;?> <?= $this->errorMessage;?></title>
    <meta name="description" content="PHP Web Services Ltd, Providing development services for advanced PHP projects." />
    <link rel="stylesheet" href="/css/main.css" />
    <!--[if lte IE 7]><link rel="/css/main-ie.css" /><![endif]-->
    <meta name=viewport content="width=device-width, initial-scale=1">
</head>

<body>
<div class="page responsive">
    <div class="outer">
        <div class="middle">
            <div class="inner">
                <h2>oh dear!</h2>
                <img alt="PHP Web Services Logo" src="/images/logo.png" />
                <h3><?= $this->errorCode;?> <?= $this->errorMessage;?></h3>
            </div>
        </div>
    </div>
</div>
</body>
</html>
