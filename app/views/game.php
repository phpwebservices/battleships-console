<div class="notifications <?= $this->get('showWarning') || $this->get('errors') ? 'warning' : '';?>">
    <?php $this->partial('errors');?>
    <?php $this->partial('messages');?>
    <?php if ($this->get('numberOfShots')) { ?>
        <?= $this->get('numberOfShots');?>
    <?php } ?>
</div>
<div class="controls">
    <?php $this->partial('control-form');?>
    <?php $this->partial('options-form');?>
</div>
<?php $this->partial('grid');?>