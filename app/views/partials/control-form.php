<form class="control-form" name="game-form" action="<?= $this->get('action');?>" method="POST">
   <div class="custom-input item1">
       <label for="new-shot"><?= "Enter coordinates example: A".($this->get('isDoubleSize') ? '05' : '5');?></label>
       <input onfocus="this.select();" id="new-shot" name="position" size="4" value="<?= $this->get('position');?>" autofocus />
       <input class="fire-btn" type="submit" value="Fire!" />
   </div>
</form>
