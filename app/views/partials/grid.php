<?php
/** @var $this \App\Controllers\Game */
/** @var $grid \App\Models\Collections\GridItemCollection */
$grid = $this->get('grid');
?>
<div class="grid-container <?= $this->get('gridAlert', false) ? 'warning' : '';?>">
    <div class="grid-header dy-font"></div>
    <?php foreach($this->get('colHeaders', []) as $header): ?>
        <div class="grid-header dy-font"><?= $header;?></div>
    <?php endforeach; ?>
    <?php foreach($this->get('rowHeaders', []) as $row): ?>
        <div class="grid-row">
            <div class="grid-item dy-font"><?= $row;?></div>
            <?php foreach ($this->get('colHeaders', []) as $col):
                /** @var $gridItem \App\Models\GridItem */
                $gridItem = $grid->offsetGet($row.$col); ?>
                <div class="grid-item dy-font type-<?= $gridItem->getType()?> status-<?= $gridItem->getStatus();?>"></div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>