<div id="console-window">
    <div class="console-content">

        <table class="table table-striped console-window">
            <thead>
            <tr>
                <th>Id</th>
                <th class="text-center">Message</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <a title="Console" onmousedown="Console.resizeDebug('console-window');" onmouseup="Console.stopResize();">&nbsp;</a>
</div>