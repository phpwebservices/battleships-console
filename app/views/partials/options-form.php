<form class="options-form" name="options-form" action="<?= $this->get('optionsAction');?>" method="POST">

       <div class="custom-input item2">
           <?php if($this->get('isDoubleSize')) { ?>
               Ships:
               <input title="Enter the number of Battleships that you would like, these ships take up 5 board units and you can have a maximum number of 8 on the board." id="battleshipsCount" name="battleshipsCount" size="2" value="<?= $this->get('battleshipsCount');?>"/>
               <input title="Enter the number of Destroyers that you would like, these ships take up 4 board units and you can have a maximum number of 8 on the board." id="destroyersCount" name="destroyersCount" size="2" value="<?= $this->get('destroyersCount');?>"/>
               <input title="Enter the number of Frigates that you would like, these ships take up 2 board units and you can have a maximum number of 8 on the board." id="frigatesCount" name="frigatesCount" size="2" value="<?= $this->get('frigatesCount');?>"/>
           <?php } ?>
           <label for="boardCols">Width:</label>
           <input title="Board Width (Max 52)" id="boardCols" name="boardCols" size="2" value="<?= $this->get('boardCols', 10);?>"/>
           <label for="boardRows">Height:</label>
           <input title="Board Height (Max 26)" id="boardRows" name="boardRows" size="2" value="<?= $this->get('boardRows', 10);?>"/>
           <input type="submit" onclick="confirm('Are you sure you would like to quit this game and start again?');" value="Start New Game"/>
       </div>

</form>