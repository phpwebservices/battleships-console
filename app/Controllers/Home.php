<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Controllers;

use App\Helpers\BoardOptions;
use App\Models\Collections\GridItemCollection;
use App\Models\Ships\Battleship;
use App\Models\Ships\Destroyer;
use App\Models\Ships\Frigate;
use App\Models\BattleshipsBoard;
use App\Models\Collections\BoardItemsCollection;
use App\Models\GameBoardPositionGenerator;
use System\Data\Drivers\Session;

class Home extends Game
{
    /**
     * @var BoardOptions
     */
    protected ?BoardOptions $boardOptions = null;

    public function postConstruct(): void
    {
        $this->boardOptions = new BoardOptions($this->request, $this->session);

        $this->boardOptions->validateNewGameInput();

        $this->data($this->boardOptions->data());
    }

    public function execute(): void
    {
        try {
            // Start new game

            $boardOptions = $this->boardOptions;

            if ($boardOptions->errors()) return;

            $this->battleshipsBoard = new BattleshipsBoard(
                $boardOptions->cols(),
                $boardOptions->rows(),
                new Session($this->session),
                new BoardItemsCollection([]),
                new GridItemCollection([])
            );

            $this->battleshipsBoard->reset();

            $this->boardOptions->setBattleshipsBoardInstance($this->battleshipsBoard);

            $this->setGridContent();
            $this->setGrid();

            $this->addBoardItems();

            if (!$this->get('errors')) {
                $this->add('messages', "New Game Created! Using {$this->boardOptions->shipCount()} ships.");
            }

        } catch(\Exception $e) {
            $this->add('errors', " {$e->getMessage()}");
        }
    }

    protected function addBoardItems(): void
    {
        $this->boardOptions->validateNewGameOptions();
        $this->data($this->boardOptions->data());

        $generator = new GameBoardPositionGenerator($this->battleshipsBoard);

        for ($i = 0; $i < $this->boardOptions->battleshipsCount(); $i++) {
            $this->battleshipsBoard->addBoardItem($generator->positionRandomItem(new Battleship()));
        }

        for ($i = 0; $i < $this->boardOptions->destroyersCount(); $i++) {
            $this->battleshipsBoard->addBoardItem($generator->positionRandomItem(new Destroyer()));
        }

        for ($i = 0; $i < $this->boardOptions->frigatesCount(); $i++) {
            $this->battleshipsBoard->addBoardItem($generator->positionRandomItem(new Frigate()));
        }
    }

}