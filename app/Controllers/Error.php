<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Controllers;

use System;

class Error extends System\BaseController
{
    /**
     * @var int
     */
    private int $errorCode = 0;

    /**
     * @var string
     */
    private string $errorMessage = '';

    public function execute(): void
    {
        header("HTTP/1.0 {$this->errorCode} {$this->errorMessage}");
    }

    /**
     * @return void
     */
    public function display(): void
    {
        require "{$this->getViewPath()}/{$this->viewName}.php";
    }

    /**
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    /**
     * @param int $errorCode
     */
    public function setErrorCode(int $errorCode): void
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage(string $errorMessage): void
    {
        $this->errorMessage = $errorMessage;
    }

}