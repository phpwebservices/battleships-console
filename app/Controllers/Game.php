<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 * Updated: 30/11/19
 */

namespace App\Controllers;

use App\Models\Collections\GridItemCollection;
use System;
use System\Data\Drivers\Session;
use App\Helpers\BoardOptions;
use App\Models\BattleshipsBoard;
use App\Models\Collections\BoardItemsCollection;

class Game extends System\BaseController
{
    /**
     * @var BattleshipsBoard
     */
    protected ?BattleshipsBoard $battleshipsBoard = null;

    /**
     * @var BoardOptions
     */
    protected ?BoardOptions $boardOptions = null;

    public function execute(): void
    {
        $this->context->permanentRedirect(''); // Redirect back to home let child classes handle execution
    }

    public function postConstruct(): void
    {
        $this->boardOptions = new BoardOptions($this->request, $this->session);

        $this->data($this->boardOptions->data());
    }

    /**
     * @throws \Exception
     */
    protected function restoreGame(): void
    {
        $this->battleshipsBoard = new BattleshipsBoard(
            $this->boardOptions->cols(),
            $this->boardOptions->rows(),
            new Session($this->session),
            new BoardItemsCollection([]),
            new GridItemCollection([])
        );
        $this->battleshipsBoard->restore();
    }

    protected function setGridHiddenContent(): void
    {
        $this->set('grid', $this->battleshipsBoard->getHiddenData());
    }

    protected function setGridContent(): void
    {
        $this->set('grid', $this->battleshipsBoard->getGrid());
    }

    protected function setGrid(): void
    {
        $this->set('colHeaders', $this->battleshipsBoard->getColHeaders());
        $this->set('rowHeaders', $this->battleshipsBoard->getRowHeaders());
    }

    public function postExecute(): void
    {
        $this->setViewName('game');
        $this->set('action', $this->context->url(['play']));
        $this->set('optionsAction', $this->context->url([]));
    }

}