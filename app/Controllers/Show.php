<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Controllers;

class Show extends Game
{
    public function execute(): void
    {
        $this->restoreGame();
        $this->setGridHiddenContent();
        $this->setGrid();
    }
}