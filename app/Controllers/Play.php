<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Controllers;

use App\Helpers\SaveShotOptions;

class Play extends Game
{
    public function execute(): void
    {
        $this->restoreGame();
        $this->saveShot();
        $this->setGridContent();
        $this->setGrid();
    }

    protected function saveShot(): void
    {
        $this->data(
            (new SaveShotOptions($this->battleshipsBoard, $this->request, $this->session, $this->context))->data()
        );
    }

}