<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 * Updated: 30/11/19
 */

namespace App\Helpers;

use System;
use App\Models\BattleshipsBoard;
use System\Request;

/**
 * Class ShipOptions
 * @package App\Helpers
 *
 * @property int boardCols read only property
 * @property int boardRows
 */
class BoardOptions extends System\ControllerHelper
{
    const MAX_LARGE_BOARDS_SHIPS = 8;

    const DEFAULT_BATTLESHIPS = 2;
    const DEFAULT_DESTROYERS = 3;
    const DEFAULT_FRIGATES = 5;

    const BOARD_COL_DEFAULT = 15;
    const BOARD_ROWS_DEFAULT = 20;

    const BOARD_MIN_SIZE_LIMIT = 10;

    /**
     * @var BattleshipsBoard
     */
    protected ?BattleshipsBoard $battleshipsBoard = null;

    /**
     * @var Request
     */
    protected ?Request $request = null;

    public function __construct(System\Request $request, System\Session $session) {
        $this->request = $request;
        $this->session = $session;

        $this->restore('boardCols', self::BOARD_MIN_SIZE_LIMIT);
        $this->restore('boardRows', self::BOARD_MIN_SIZE_LIMIT);

        $this->restore('battleshipsCount', self::DEFAULT_BATTLESHIPS);
        $this->restore('destroyersCount', self::DEFAULT_DESTROYERS);
        $this->restore('frigatesCount', self::DEFAULT_FRIGATES);

        $this->restore('isDoubleSize', false);
        $this->restore('ratioOfBoardSize', '0.8');

        $this->restore('position', '', function ($data) {
            return strtoupper(substr($data, 0, 3));
        });
    }

    public function setBattleshipsBoardInstance(BattleshipsBoard $battleshipsBoard) {
        $this->battleshipsBoard = $battleshipsBoard;
    }

    public function validateNewGameInput() {
        if ($this->cols() < self::BOARD_MIN_SIZE_LIMIT) {
            $this->add('errors', "Board columns can not be set to less than " . self::BOARD_MIN_SIZE_LIMIT.',');
        }

        if ($this->rows() < self::BOARD_MIN_SIZE_LIMIT) {
            $this->add('errors', "Board rows can not be set to less that " . self::BOARD_MIN_SIZE_LIMIT);
        }
    }

    public function validateNewGameOptions() {
        if ($this->battleshipsBoard->isDoubleSize()) {

            $this->set('isDoubleSize', true);

            if ($this->battleshipsCount() > self::MAX_LARGE_BOARDS_SHIPS
                || $this->destroyersCount() > self::MAX_LARGE_BOARDS_SHIPS
                || $this->frigatesCount() > self::MAX_LARGE_BOARDS_SHIPS
            ) {
                $this->add("errors",
                    "Max " . self::MAX_LARGE_BOARDS_SHIPS . " of each ships allowed on large boards");
                return;
            }

            if ($this->battleshipsCount() < 1 || $this->destroyersCount() < 1 || $this->frigatesCount() < 1) {
                $this->add('errors', "You must enter at least one of each ship to play battleships");
                return;
            }

        } else {
            $this->set('isDoubleSize', false);
            $this->set('battleshipsCount', self::DEFAULT_BATTLESHIPS);
            $this->set('destroyersCount', self::DEFAULT_DESTROYERS);
            $this->set('frigatesCount', self::DEFAULT_FRIGATES);
        }
        
        $this->set('ratioOfBoardSize', $this->battleshipsBoard->ratioOfBoardSize());
    }

    public function errors(): array {
        return $this->get('errors', []);
    }

    public function position(): string {
        return $this->get('position');
    }

    public function battleshipsCount(): int {
        return $this->get('battleshipsCount');
    }

    public function destroyersCount(): int {
        return $this->get('destroyersCount');
    }

    public function frigatesCount(): int {
        return $this->get('frigatesCount');
    }

    public function cols(): int {
        return $this->get('boardCols');
    }

    public function rows(): int {
        return $this->get('boardRows');
    }

    public function shipCount(): int {
        return $this->battleshipsCount() + $this->destroyersCount() + $this->frigatesCount();
    }

    public function __destruct() {
        $this->session->set('boardCols', $this->cols());
        $this->session->set('boardRows', $this->rows());
        $this->session->set('battleshipsCount', $this->battleshipsCount());
        $this->session->set('destroyersCount', $this->destroyersCount());
        $this->session->set('frigatesCount', $this->frigatesCount());
        $this->session->set('isDoubleSize', $this->get('isDoubleSize'));
        $this->session->set('ratioOfBoardSize', $this->get('ratioOfBoardSize'));
        $this->session->set('position', $this->get('position'));
    }

}