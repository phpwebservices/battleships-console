<?php
/**
 * Company: PHP Web Services Ltd
 * User: Chris Cunningham
 * Date: 26/04/15
 */

namespace App\Helpers;

use System;
use App\Models\BattleshipsBoard;
use App\Models\Ships\Ship;

class SaveShotOptions extends System\BaseClass
{
    /**
     * @var BattleshipsBoard
     */
    protected ?BattleshipsBoard $battleshipsBoard = null;

    /**
     * @var System\Session
     */
    protected ?System\Session $session = null;

    /**
     * SaveShotOptions constructor.
     * @param BattleshipsBoard $battleshipsBoard
     * @param System\Request $request
     * @param System\Session $session
     * @param System\Context $context
     */
    public function __construct(
        BattleshipsBoard $battleshipsBoard, System\Request $request, System\Session $session, System\Context $context
    ) {

        $this->battleshipsBoard = $battleshipsBoard;
        $this->session = $session;

        try {
            $position = strtoupper($request->get('position'));

            // If the game player has completed the game and tries to make a shot
            // on the same board Then redirect to home and start a fresh game
            if ($session->get('isCompleted')) {
                $session->set('isCompleted', false);
                $context->permanentRedirect('');
            }

            $ship = $this->battleshipsBoard->positionMatch($position);

            if ($ship instanceof Ship) {

                if ($ship->isAlreadyHit($position)) {
                    $this->add('messages', "Ship has already been hit at: {$position}.");
                    return true;
                }

                $ship->markHit($position);

                $this->battleshipsBoard->setGridState(
                    $position,
                    BattleshipsBoard::GRID_STATUS_HIT,
                    $ship->type()
                );

                $this->add('messages', "{$ship->classShortName()}! Has been hit.");

                $this->add('gridAlert', true);

                if($ship->isDestroyed()) {
                    $this->add('showWarning', true);
                    $this->add('messages', "Well done, you have sunk the computer's {$ship->classShortName()}.");
                }

            } else {
                $this->battleshipsBoard->setGridState(
                    $position,
                    BattleshipsBoard::GRID_STATUS_MISS,
                    BattleshipsBoard::TYPE_MISS
                );
                $this->add('messages', "Miss!");
            }

            if ($this->battleshipsBoard->isCompleted()) {
                $this->add('showWarning', true);
                $this->add('gridAlert', true);
                $this->add('messages', "Congratulations! you have destroyed all of the computers ship's you are the winner!");
                $this->set('numberOfShots', "Well done! You completed the game in {$this->battleshipsBoard->getNumberOfShots()} shots.");
            } else {
                $this->set('numberOfShots', "Shots Taken: {$this->battleshipsBoard->getNumberOfShots()}");
            }


        } catch (\Exception $e) {
            $this->add('messages', $e->getMessage());
        }
    }

    public function __destruct()
    {
        if ($this->battleshipsBoard->isCompleted()) {
            $this->session->set('isCompleted', true);
        }
    }
}